# Clock_Timer

## Purpose
The purpose of this device is to calculate the passed time till an event which is setted in the past (should work also for the future, but haven't tried that tbh). 

All neccessary data is retrieved from a NTP-Server via W-LAN. Which need to be set in the bootmenu of the device.

## Arduino IDE Setup
* Board: "NodeMCU 1.0 (ESP-12E Module
* Upload Speed: "5120000"
* CPU Frequency: "160 MHz"
* Flash Size: 4MB (FS: 2MB OTA: ~1019KB)

## Functionality
* Micro-USB port for debugging and programming
  * Current Baudrate: 9600,8,n,1
* 5V socket for powersupply
  * 5V @ 4A Power Supply
* Resetswitch (directly beside the power socket)
* Colour and Brightness switch (at the top row of the housing)

## Presentation
The display will show three rows with date, where the top row is the time, the second row the date, and the third row the calculated date.

![Front View with ranndom time](/sources/images/front.jpg)
![Backview](/sources/images/back.jpg)

## Used Parts
* RGB LED Matrix 64x32 (or similar)
* ESP8266 Wifi Module (AZDelivery NodeMCU Lolin V3)
* Custom made Ccrrierboard (see attached schematics)
* Pushbuttons
* Custom made wood housing

## Bootup Sequence
During the bootup there are a (currently) 3 seconds time delay to enter specifig datas like:
* W-LAN SSID
* W-LAN Password
* Time calcluated date (Day, Month, Year)
The entered data will be shown on the screen and saved in the internal EEPROM.

## Known Quirks
* The software needs a bit of a bootup, so don't expect to get the time immediatly.
* Due to the EEPROM lifecycle limitations the current settings of colour and brigthness will be stored after 1 minute and only when it was changed.
* There is no encryption on the EEPROM what so ever, neither on the serial port. Therefore, if the device should be a gift... better overwrite SSID and Password with some random stuff.

## License
Well... I tried to put every source I found in there, the schematics are free to use. The code is written by not a professional, so don't expect high octane code. Free to use, free to fix.
