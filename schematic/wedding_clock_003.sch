EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L OwnBib:NodeMCU_LUA_Amica_V2 U1
U 1 1 5F373EEB
P 4550 3300
F 0 "U1" H 5400 3565 50  0000 C CNN
F 1 "NodeMCU_LUA_Amica_V2" H 5400 3474 50  0000 C CNN
F 2 "OwnBib:NodeMCU_LUA_Amica_V2" H 4550 3300 50  0001 C CNN
F 3 "" H 4550 3300 50  0001 C CNN
F 4 "3M" H 4550 3300 50  0001 C CNN "Hersteller"
F 5 "929974E-01-15-ND " H 4550 3300 50  0001 C CNN "Typ"
F 6 "2" H 4550 3300 50  0001 C CNN "Anzahl"
	1    4550 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 6450 3000 6450
Wire Wire Line
	3550 6850 3000 6850
Wire Wire Line
	3550 6950 3000 6950
Wire Wire Line
	3550 7050 3000 7050
Wire Wire Line
	3550 7150 3000 7150
Wire Wire Line
	4600 7050 4050 7050
Wire Wire Line
	4600 6950 4050 6950
Wire Wire Line
	4600 6850 4050 6850
Wire Wire Line
	4600 6750 4050 6750
NoConn ~ 5450 6750
NoConn ~ 5450 6850
NoConn ~ 5450 6950
NoConn ~ 5450 7050
NoConn ~ 5450 7150
NoConn ~ 5950 7150
NoConn ~ 5950 7050
NoConn ~ 5950 6950
NoConn ~ 5950 6850
NoConn ~ 5950 6750
NoConn ~ 5950 6550
Wire Wire Line
	6800 3300 6250 3300
Wire Wire Line
	6800 3400 6250 3400
Wire Wire Line
	6800 3500 6250 3500
Wire Wire Line
	6800 3600 6250 3600
Wire Wire Line
	6800 3700 6250 3700
Wire Wire Line
	6800 4000 6250 4000
Wire Wire Line
	6800 4100 6250 4100
Wire Wire Line
	6800 4200 6250 4200
Wire Wire Line
	6800 4300 6250 4300
Text Label 3050 6450 0    50   ~ 0
R0
Text Label 3050 7050 0    50   ~ 0
CLK
Text Label 3050 7150 0    50   ~ 0
OE
Text Label 4250 6850 0    50   ~ 0
B
Text Label 3050 6850 0    50   ~ 0
A
Text Label 4250 7050 0    50   ~ 0
STB
Wire Wire Line
	4050 7150 4150 7150
Wire Wire Line
	4150 7150 4150 7300
$Comp
L power:GND #PWR012
U 1 1 5F3B5AA7
P 4150 7300
F 0 "#PWR012" H 4150 7050 50  0001 C CNN
F 1 "GND" H 4155 7127 50  0000 C CNN
F 2 "" H 4150 7300 50  0001 C CNN
F 3 "" H 4150 7300 50  0001 C CNN
	1    4150 7300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5F3B652F
P 4450 4900
F 0 "#PWR08" H 4450 4650 50  0001 C CNN
F 1 "GND" H 4455 4727 50  0000 C CNN
F 2 "" H 4450 4900 50  0001 C CNN
F 3 "" H 4450 4900 50  0001 C CNN
	1    4450 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5F3B84CD
P 6350 4950
F 0 "#PWR09" H 6350 4700 50  0001 C CNN
F 1 "GND" H 6355 4777 50  0000 C CNN
F 2 "" H 6350 4950 50  0001 C CNN
F 3 "" H 6350 4950 50  0001 C CNN
	1    6350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 4600 6350 4600
Wire Wire Line
	6350 4600 6350 4950
Wire Wire Line
	6250 3900 6350 3900
Wire Wire Line
	6350 3900 6350 4600
Connection ~ 6350 4600
Wire Wire Line
	4550 4600 4450 4600
Wire Wire Line
	4450 4600 4450 4900
Wire Wire Line
	4550 4200 4450 4200
Wire Wire Line
	4450 4200 4450 4600
Connection ~ 4450 4600
$Comp
L power:+3V3 #PWR011
U 1 1 5F3CE1A6
P 1850 2650
F 0 "#PWR011" H 1850 2500 50  0001 C CNN
F 1 "+3V3" H 1865 2823 50  0000 C CNN
F 2 "" H 1850 2650 50  0001 C CNN
F 3 "" H 1850 2650 50  0001 C CNN
	1    1850 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 5F3D0CDA
P 4400 3100
F 0 "#PWR03" H 4400 2950 50  0001 C CNN
F 1 "+3V3" H 4415 3273 50  0000 C CNN
F 2 "" H 4400 3100 50  0001 C CNN
F 3 "" H 4400 3100 50  0001 C CNN
	1    4400 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4300 4400 4300
Wire Wire Line
	4400 4300 4400 3100
NoConn ~ 4550 4400
Wire Wire Line
	4550 4700 4150 4700
$Comp
L power:+5V #PWR02
U 1 1 5F3DE291
P 4150 2700
F 0 "#PWR02" H 4150 2550 50  0001 C CNN
F 1 "+5V" H 4165 2873 50  0000 C CNN
F 2 "" H 4150 2700 50  0001 C CNN
F 3 "" H 4150 2700 50  0001 C CNN
	1    4150 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4500 3700 4500
Text Label 3800 4500 0    50   ~ 0
nRST
Text Label 2050 4600 0    50   ~ 0
nRST
$Comp
L power:GND #PWR06
U 1 1 5F3FC7A4
P 1950 5050
F 0 "#PWR06" H 1950 4800 50  0001 C CNN
F 1 "GND" H 1955 4877 50  0000 C CNN
F 2 "" H 1950 5050 50  0001 C CNN
F 3 "" H 1950 5050 50  0001 C CNN
	1    1950 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 5050 1950 5000
Wire Wire Line
	1950 5000 1650 5000
Wire Wire Line
	1950 5000 1950 4950
Connection ~ 1950 5000
Wire Wire Line
	1650 4900 1750 4900
NoConn ~ 4550 3600
NoConn ~ 4550 3700
NoConn ~ 4550 3800
NoConn ~ 4550 3900
NoConn ~ 4550 4000
NoConn ~ 4550 4100
Wire Wire Line
	1650 3200 1850 3200
Wire Wire Line
	1850 3050 1850 3200
Wire Wire Line
	1850 2650 1850 2700
Wire Wire Line
	1850 2700 2150 2700
Wire Wire Line
	2150 2700 2150 2750
Connection ~ 1850 2700
Wire Wire Line
	1850 2700 1850 2750
Wire Wire Line
	4550 3300 3700 3300
$Comp
L power:GND #PWR05
U 1 1 5F52C877
P 1750 1800
F 0 "#PWR05" H 1750 1550 50  0001 C CNN
F 1 "GND" H 1755 1627 50  0000 C CNN
F 2 "" H 1750 1800 50  0001 C CNN
F 3 "" H 1750 1800 50  0001 C CNN
	1    1750 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1350 1750 1350
Connection ~ 1750 1700
Wire Wire Line
	1750 1700 1750 1800
Wire Wire Line
	1650 1600 1800 1600
Wire Wire Line
	1800 1600 1800 1250
Wire Wire Line
	1800 1250 1650 1250
$Comp
L power:+5V #PWR01
U 1 1 5F53E88D
P 1800 1150
F 0 "#PWR01" H 1800 1000 50  0001 C CNN
F 1 "+5V" H 1815 1323 50  0000 C CNN
F 2 "" H 1800 1150 50  0001 C CNN
F 3 "" H 1800 1150 50  0001 C CNN
	1    1800 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1250 1800 1150
Connection ~ 1800 1250
$Comp
L Device:C C1
U 1 1 5F59FD22
P 2150 1500
F 0 "C1" H 2265 1546 50  0000 L CNN
F 1 "4,7µF" H 2265 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 2188 1350 50  0001 C CNN
F 3 "~" H 2150 1500 50  0001 C CNN
F 4 "‎Taiyo Yuden‎" H 2150 1500 50  0001 C CNN "Hersteller"
F 5 "UMK325B7475KM-P" H 2150 1500 50  0001 C CNN "Typ"
	1    2150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1700 1750 1700
Wire Wire Line
	1750 1350 1750 1700
Wire Wire Line
	1750 1700 2150 1700
Wire Wire Line
	2150 1700 2150 1650
Wire Wire Line
	2150 1700 2600 1700
Wire Wire Line
	2600 1700 2600 1650
Connection ~ 2150 1700
Wire Wire Line
	1800 1250 2150 1250
Wire Wire Line
	2150 1250 2150 1350
Wire Wire Line
	2150 1250 2600 1250
Wire Wire Line
	2600 1250 2600 1350
Connection ~ 2150 1250
$Comp
L Device:C C2
U 1 1 5F6905F9
P 2600 1500
F 0 "C2" H 2715 1546 50  0000 L CNN
F 1 "100nF" H 2715 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2638 1350 50  0001 C CNN
F 3 "~" H 2600 1500 50  0001 C CNN
F 4 "‎KEMET‎" H 2600 1500 50  0001 C CNN "Hersteller"
F 5 "C0603C104Z3VACTU" H 2600 1500 50  0001 C CNN "Typ"
	1    2600 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 4600 1950 4600
$Comp
L Device:C C4
U 1 1 5F6A2001
P 1950 4800
F 0 "C4" H 2065 4846 50  0000 L CNN
F 1 "100nF" H 2065 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1988 4650 50  0001 C CNN
F 3 "~" H 1950 4800 50  0001 C CNN
F 4 "Yageo" H 1950 4800 50  0001 C CNN "Hersteller"
F 5 "C0603C104Z3VACTU" H 1950 4800 50  0001 C CNN "Typ"
	1    1950 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 4650 1950 4600
Connection ~ 1950 4600
Wire Wire Line
	1950 4600 2250 4600
Wire Wire Line
	1750 4600 1750 4900
$Comp
L Device:R R2
U 1 1 5F6BFE88
P 1850 2900
F 0 "R2" H 1920 2946 50  0000 L CNN
F 1 "39k" H 1920 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 2900 50  0001 C CNN
F 3 "~" H 1850 2900 50  0001 C CNN
F 4 "TE Connectivity Passive Product " H 1850 2900 50  0001 C CNN "Hersteller"
F 5 "CRGCQ0603F39K" H 1850 2900 50  0001 C CNN "Typ"
	1    1850 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F6C5E00
P 2150 2900
F 0 "R3" H 2220 2946 50  0000 L CNN
F 1 "56k" H 2220 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2080 2900 50  0001 C CNN
F 3 "~" H 2150 2900 50  0001 C CNN
F 4 "Yageo " H 2150 2900 50  0001 C CNN "Hersteller"
F 5 "RC0201FR-0756KL" H 2150 2900 50  0001 C CNN "Typ"
	1    2150 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5F375A2C
P 1450 1250
F 0 "J1" H 1600 1250 50  0000 C CNN
F 1 "Conn_01x02" H 1800 1150 50  0000 C CNN
F 2 "OwnBib:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1450 1250 50  0001 C CNN
F 3 "~" H 1450 1250 50  0001 C CNN
F 4 "Phoenix Contact " H 1450 1250 50  0001 C CNN "Hersteller"
F 5 "1989447 " H 1450 1250 50  0001 C CNN "Typ"
	1    1450 1250
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5F727964
P 1450 1600
F 0 "J2" H 1600 1600 50  0000 C CNN
F 1 "Conn_01x02" H 1800 1500 50  0000 C CNN
F 2 "OwnBib:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1450 1600 50  0001 C CNN
F 3 "~" H 1450 1600 50  0001 C CNN
F 4 "Phoenix Contact " H 1450 1600 50  0001 C CNN "Herstelller"
F 5 "1989447" H 1450 1600 50  0001 C CNN "Typ"
	1    1450 1600
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5F72DA4F
P 1450 4900
F 0 "J3" H 1600 4900 50  0000 C CNN
F 1 "Conn_01x02" H 1800 4800 50  0000 C CNN
F 2 "OwnBib:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1450 4900 50  0001 C CNN
F 3 "~" H 1450 4900 50  0001 C CNN
F 4 "Phoenix Contact " H 1450 4900 50  0001 C CNN "Hersteller"
F 5 "1989447" H 1450 4900 50  0001 C CNN "Typ"
	1    1450 4900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5F74BD56
P 1450 3200
F 0 "J5" H 1600 3200 50  0000 C CNN
F 1 "Conn_01x02" H 1800 3100 50  0000 C CNN
F 2 "OwnBib:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1450 3200 50  0001 C CNN
F 3 "~" H 1450 3200 50  0001 C CNN
F 4 "Phoenix Contact " H 1450 3200 50  0001 C CNN "Hersteller"
F 5 "1989447" H 1450 3200 50  0001 C CNN "Typ"
	1    1450 3200
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 5F751C9D
P 1450 3550
F 0 "J8" H 1600 3550 50  0000 C CNN
F 1 "Conn_01x02" H 1800 3450 50  0000 C CNN
F 2 "OwnBib:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1450 3550 50  0001 C CNN
F 3 "~" H 1450 3550 50  0001 C CNN
F 4 "Phoenix Contact " H 1450 3550 50  0001 C CNN "Hersteller"
F 5 "1989447" H 1450 3550 50  0001 C CNN "Typ"
	1    1450 3550
	-1   0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5F75E693
P 950 7200
F 0 "H1" H 1050 7246 50  0000 L CNN
F 1 "MountingHole" H 1050 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_DIN965_Pad" H 950 7200 50  0001 C CNN
F 3 "~" H 950 7200 50  0001 C CNN
	1    950  7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F75F8B9
P 950 7450
F 0 "H3" H 1050 7496 50  0000 L CNN
F 1 "MountingHole" H 1050 7405 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 950 7450 50  0001 C CNN
F 3 "~" H 950 7450 50  0001 C CNN
	1    950  7450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F765BBC
P 1800 7200
F 0 "H2" H 1900 7246 50  0000 L CNN
F 1 "MountingHole" H 1900 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_DIN965_Pad" H 1800 7200 50  0001 C CNN
F 3 "~" H 1800 7200 50  0001 C CNN
	1    1800 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F76BB65
P 1800 7450
F 0 "H4" H 1900 7496 50  0000 L CNN
F 1 "MountingHole" H 1900 7405 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 1800 7450 50  0001 C CNN
F 3 "~" H 1800 7450 50  0001 C CNN
	1    1800 7450
	1    0    0    -1  
$EndComp
NoConn ~ 6250 3800
NoConn ~ 6250 4700
Connection ~ 2600 1700
Wire Wire Line
	3450 1700 3450 1650
Connection ~ 2600 1250
Wire Wire Line
	3450 1350 3450 1250
$Comp
L Device:D_Zener D1
U 1 1 5F5EF589
P 3450 1500
F 0 "D1" V 3404 1579 50  0000 L CNN
F 1 "D_Zener" V 3495 1579 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 3450 1500 50  0001 C CNN
F 3 "~" H 3450 1500 50  0001 C CNN
F 4 "ON Semiconductor" V 3450 1500 50  0001 C CNN "Hersteller"
F 5 "MM3Z5V6ST1G" V 3450 1500 50  0001 C CNN "Typ"
	1    3450 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	3050 1700 3450 1700
Wire Wire Line
	2600 1700 3050 1700
Connection ~ 3050 1700
Wire Wire Line
	3050 1700 3050 1650
$Comp
L Device:C C3
U 1 1 5F69646B
P 3050 1500
F 0 "C3" H 3165 1546 50  0000 L CNN
F 1 "10pF" H 3165 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3088 1350 50  0001 C CNN
F 3 "~" H 3050 1500 50  0001 C CNN
F 4 "Yageo‎" H 3050 1500 50  0001 C CNN "Hersteller"
F 5 "CC0603JRNPO9BN100" H 3050 1500 50  0001 C CNN "Typ"
	1    3050 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1250 3050 1250
Wire Wire Line
	2600 1250 3050 1250
Connection ~ 3050 1250
Wire Wire Line
	3050 1250 3050 1350
Text Label 3050 6950 0    50   ~ 0
C
Text Label 4250 6950 0    50   ~ 0
D
Text Label 4250 6750 0    50   ~ 0
E
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 5F3ADBC6
P 1500 6300
F 0 "J9" H 1650 6300 50  0000 C CNN
F 1 "Conn_01x02" H 1850 6200 50  0000 C CNN
F 2 "OwnBib:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1500 6300 50  0001 C CNN
F 3 "~" H 1500 6300 50  0001 C CNN
F 4 "Phoenix Contact" H 1500 6300 50  0001 C CNN "Hersteller"
F 5 "1989447" H 1500 6300 50  0001 C CNN "Typ"
	1    1500 6300
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5F3B1F40
P 1900 6100
F 0 "R4" H 1970 6146 50  0000 L CNN
F 1 "0" H 1970 6055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1830 6100 50  0001 C CNN
F 3 "~" H 1900 6100 50  0001 C CNN
F 4 "Yageo " H 1900 6100 50  0001 C CNN "Hersteller"
F 5 "RC0603JR-070RL" H 1900 6100 50  0001 C CNN "Typ"
	1    1900 6100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5F3C3031
P 1900 5850
F 0 "#PWR0101" H 1900 5700 50  0001 C CNN
F 1 "+5V" H 1915 6023 50  0000 C CNN
F 2 "" H 1900 5850 50  0001 C CNN
F 3 "" H 1900 5850 50  0001 C CNN
	1    1900 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F3CFCB2
P 1900 6450
F 0 "#PWR0102" H 1900 6200 50  0001 C CNN
F 1 "GND" H 1905 6277 50  0000 C CNN
F 2 "" H 1900 6450 50  0001 C CNN
F 3 "" H 1900 6450 50  0001 C CNN
	1    1900 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 6400 1900 6400
Wire Wire Line
	1700 6300 1900 6300
Wire Wire Line
	1900 6300 1900 6250
Wire Wire Line
	1900 5950 1900 5900
$Comp
L Device:R R5
U 1 1 5F3E30D2
P 2150 6100
F 0 "R5" H 2220 6146 50  0000 L CNN
F 1 "0" H 2220 6055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2080 6100 50  0001 C CNN
F 3 "~" H 2150 6100 50  0001 C CNN
F 4 "Yageo" H 2150 6100 50  0001 C CNN "Hersteller"
F 5 "RC0603JR-070RL" H 2150 6100 50  0001 C CNN "Typ"
	1    2150 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 5900 2150 5900
Wire Wire Line
	2150 5900 2150 5950
Connection ~ 1900 5900
Wire Wire Line
	1900 5900 1900 5850
Wire Wire Line
	1900 6300 2150 6300
Wire Wire Line
	2150 6300 2150 6250
Connection ~ 1900 6300
$Comp
L Device:R R1
U 1 1 5F3D7FA0
P 1850 3900
F 0 "R1" H 1920 3946 50  0000 L CNN
F 1 "10k" H 1920 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 3900 50  0001 C CNN
F 3 "~" H 1850 3900 50  0001 C CNN
F 4 "Yageo " H 1850 3900 50  0001 C CNN "Hersteller"
F 5 "RC0603FR-0710KL" H 1850 3900 50  0001 C CNN "Typ"
	1    1850 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3550 2150 3550
Wire Wire Line
	2150 3550 2150 3050
$Comp
L power:GND #PWR0103
U 1 1 5F4087D9
P 1850 4100
F 0 "#PWR0103" H 1850 3850 50  0001 C CNN
F 1 "GND" H 1855 3927 50  0000 C CNN
F 2 "" H 1850 4100 50  0001 C CNN
F 3 "" H 1850 4100 50  0001 C CNN
	1    1850 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6450 1900 6400
Wire Wire Line
	1850 4050 1850 4100
Wire Wire Line
	1650 3650 1850 3650
Wire Wire Line
	1850 3750 1850 3650
Connection ~ 1850 3650
Wire Wire Line
	1650 3300 1850 3300
Wire Wire Line
	1850 3300 1850 3650
Text Label 2400 3650 0    50   ~ 0
switch
Text Label 3800 3300 0    50   ~ 0
switch
NoConn ~ 6250 4400
NoConn ~ 6250 4500
NoConn ~ 4050 6550
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J6
U 1 1 5F482A01
P 3750 6750
F 0 "J6" H 3800 7267 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 3800 7176 50  0000 C CNN
F 2 "Connector_Multicomp:Multicomp_MC9A12-1634_2x08_P2.54mm_Vertical" H 3750 6750 50  0001 C CNN
F 3 "~" H 3750 6750 50  0001 C CNN
F 4 "On Shore Technology Inc. " H 3750 6750 50  0001 C CNN "Hersteller"
F 5 "302-S161" H 3750 6750 50  0001 C CNN "Typ"
	1    3750 6750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J7
U 1 1 5F48440D
P 5650 6750
F 0 "J7" H 5700 7267 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 5700 7176 50  0000 C CNN
F 2 "Connector_Multicomp:Multicomp_MC9A12-1634_2x08_P2.54mm_Vertical" H 5650 6750 50  0001 C CNN
F 3 "~" H 5650 6750 50  0001 C CNN
F 4 "On Shore Technology Inc. " H 5650 6750 50  0001 C CNN "Hersteller"
F 5 "302-S161 " H 5650 6750 50  0001 C CNN "Typ"
	1    5650 6750
	1    0    0    -1  
$EndComp
Text Label 5150 6450 0    50   ~ 0
R1
Text Label 5150 6550 0    50   ~ 0
B1
Text Label 4250 6650 0    50   ~ 0
G1
Text Label 4250 6450 0    50   ~ 0
G0
$Comp
L Device:D_Schottky D2
U 1 1 5F4ADED6
P 4150 2900
F 0 "D2" V 4200 3400 50  0000 R CNN
F 1 "D_Schottky" V 4100 3400 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-123F" H 4150 2900 50  0001 C CNN
F 3 "~" H 4150 2900 50  0001 C CNN
F 4 "ON Semiconductor " V 4150 2900 50  0001 C CNN "Hersteller"
F 5 "NRVB120VLSFT1G" V 4150 2900 50  0001 C CNN "Typ"
	1    4150 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 2700 4150 2750
Wire Wire Line
	4150 3050 4150 4700
Wire Wire Line
	1850 3650 2800 3650
$Comp
L power:GND #PWR04
U 1 1 5F4E4890
P 2800 4100
F 0 "#PWR04" H 2800 3850 50  0001 C CNN
F 1 "GND" H 2805 3927 50  0000 C CNN
F 2 "" H 2800 4100 50  0001 C CNN
F 3 "" H 2800 4100 50  0001 C CNN
	1    2800 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4100 2800 4000
Wire Wire Line
	2800 3700 2800 3650
Connection ~ 2800 3650
Wire Wire Line
	2800 3650 3150 3650
Text Label 6600 4200 0    50   ~ 0
R0
Text Label 6600 4000 0    50   ~ 0
CLK
Text Label 6600 3700 0    50   ~ 0
OE
Text Label 6600 3400 0    50   ~ 0
A
Text Label 6600 4300 0    50   ~ 0
C
Text Label 6600 3500 0    50   ~ 0
B
Text Label 6600 3300 0    50   ~ 0
STB
Text Label 6600 4100 0    50   ~ 0
D
Text Label 6600 3600 0    50   ~ 0
E
$Comp
L Device:D_Zener D3
U 1 1 5F4A72CD
P 2800 3850
F 0 "D3" V 2754 3929 50  0000 L CNN
F 1 "D_Zener" V 2845 3929 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 2800 3850 50  0001 C CNN
F 3 "~" H 2800 3850 50  0001 C CNN
F 4 "Micro Commercial Co " V 2800 3850 50  0001 C CNN "Hersteller"
F 5 "MMSZ4678-TP" V 2800 3850 50  0001 C CNN "Typ"
	1    2800 3850
	0    1    1    0   
$EndComp
Text Notes 8850 1150 0    50   ~ 0
Revisionshistorie\n001:    Initialer Schaltplan\n002:    Wechsel von ATmega Embedded zu ESP8266\n003:    Korrektur der Pinbelegung zu Display\n           Z-Diode an Analogeingang plaziert\n           Diode für 5V Versorgung von ESP8266
Text Notes 850  6950 0    50   ~ 0
Befestigung
Text Notes 850  800  0    50   ~ 0
Spannugnsversorgung
Text Notes 850  2400 0    50   ~ 0
Schalter
Text Notes 850  5550 0    50   ~ 0
Versorgung für LEDs an Schalter
Text Notes 2800 5900 0    50   ~ 0
Anschlüsse für Display
Text Notes 3550 2350 0    50   ~ 0
Beschaltung ESP8266
Wire Notes Line
	6650 7600 2700 7600
Wire Notes Line
	2700 7600 2700 5750
Wire Notes Line
	2700 5750 6650 5750
Wire Notes Line
	6650 5750 6650 7600
Wire Notes Line
	2700 6000 3750 6000
Wire Notes Line
	3750 6000 3750 5750
Wire Notes Line
	8750 600  8750 2050
Wire Notes Line
	8750 2050 11100 2050
Wire Notes Line
	11100 2050 11100 600 
Wire Notes Line
	11100 600  8750 600 
Wire Notes Line
	800  7600 2500 7600
Wire Notes Line
	2500 7600 2500 6850
Wire Notes Line
	2500 6850 800  6850
Wire Notes Line
	800  7000 1350 7000
Wire Notes Line
	1350 7000 1350 6850
Wire Notes Line
	800  6850 800  7600
Wire Notes Line
	800  5400 800  6700
Wire Notes Line
	800  6700 2400 6700
Wire Notes Line
	2400 6700 2400 5400
Wire Notes Line
	2400 5400 800  5400
Wire Notes Line
	800  5600 2200 5600
Wire Notes Line
	2200 5600 2200 5400
Wire Notes Line
	800  700  800  2100
Wire Notes Line
	800  2100 3950 2100
Wire Notes Line
	3950 2100 3950 700 
Wire Notes Line
	800  700  3950 700 
Wire Notes Line
	800  850  1750 850 
Wire Notes Line
	1750 850  1750 700 
Wire Notes Line
	800  2250 800  5300
Wire Notes Line
	800  5300 3300 5300
Wire Notes Line
	3300 5300 3300 2250
Wire Notes Line
	3300 2250 800  2250
Wire Notes Line
	800  2500 1250 2500
Wire Notes Line
	1250 2500 1250 2250
Wire Notes Line
	3500 2250 3500 5250
Wire Notes Line
	3500 5250 6900 5250
Wire Notes Line
	6900 5250 6900 2250
Wire Notes Line
	6900 2250 3500 2250
Wire Notes Line
	3500 2400 4450 2400
Wire Notes Line
	4450 2400 4450 2250
Text Label 3050 6550 0    50   ~ 0
B0
Text Label 3050 6650 0    50   ~ 0
R1
Text Label 3050 6750 0    50   ~ 0
B1
Text Label 5150 6650 0    50   ~ 0
G0
Text Label 6050 6650 0    50   ~ 0
B0
Text Label 6050 6450 0    50   ~ 0
G1
Wire Wire Line
	4600 6650 4050 6650
Wire Wire Line
	4600 6450 4050 6450
Wire Wire Line
	5450 6450 4900 6450
Wire Wire Line
	5450 6550 4900 6550
Wire Wire Line
	5450 6650 4900 6650
Wire Wire Line
	6500 6650 5950 6650
Wire Wire Line
	6500 6450 5950 6450
Wire Wire Line
	3000 6750 3550 6750
Wire Wire Line
	3000 6550 3550 6550
Wire Wire Line
	3000 6650 3550 6650
$EndSCHEMATC
