//  -------------------------------------------
//  To-Do's
//  -------------------------------------------
//  - Schreiben von Daten auf EEPROM
//  - Lesen von Eingaben von Serieller Schnittstelle    Fertig
//  - Einwahl in WLAN Netzwerk                          Fertig
//  - Uhrzeit über WLAN ermitteln                       Fertig
//  - LDR implementieren                                Entfällt weil zu wenige IOs



//  -------------------------------------------
//  Quellen
//  -------------------------------------------
// NTP Anteil: https://www.arduinoclub.de/2016/05/07/arduino-ide-esp8266-ntp-server-timezone/
// ADC Anteil: https://randomnerdtutorials.com/esp8266-adc-reading-analog-values-with-nodemcu/
// Zeitdifferenz: https://forum.arduino.cc/t/datetime-calculations/354776/8


//  -------------------------------------------
//  Includes
//  -------------------------------------------
#include <string>
#include <iostream>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
//#include <EEPROM.h>
#include <ESP_EEPROM.h>
#include <WiFiClient.h>
#include <TimeLib.h>
#include <Timezone.h>
#include <PxMatrix.h>
#include <Ticker.h>
#include <Adafruit_GFX.h>
#include <Fonts/FreeMono9pt7b.h>
#include <Fonts/FreeMonoBold9pt7b.h>

//23.09.2018

//  -------------------------------------------
//  Variablen
//  -------------------------------------------
String buff;                                // Allgemeiner String Puffer
String WLAN_SSID;                           // Globale Variable für WLAN SSID
String WLAN_PASSWORD;                       // Globale Variable für WLAN Passwort
unsigned int boot_time = 5000;              // Wartezeit in der Bootvorgang unterbrochen werden kann in ms
unsigned int address_counter = 0;           // Address Zähler für EEPROM
unsigned long NTP_Millis = 0;               // NTP Zwischenpuffer
unsigned long Update_Millis = 0;            // Update Zwischenpuffer
const long interval = 10000;                // Zeit nach der neue NTP Zeit erfragt wird
volatile bool wasConnected = false;         // Flag für WLAN-Verbindung
unsigned int sensorValue = 0;               // ADC Variable
unsigned int outputValue = 0;               // ADC Variable übersetzt
const int analogInPin = A0;                 // Analog Pin
unsigned int switch_val = 0;                // Default switch
unsigned int switch_tmp = 0;                // Tmp switch Wert
unsigned int switch_cycle = 0;              // Aktuelle Zyklen eines Schalters
unsigned int debounce = 500;                // Anzahl der maximalen Durchläufe bis Druck erkannt wird
unsigned int menu = 0;                      // Aktueller Menüpunkt
unsigned int menu_tmp = 0;              
unsigned int colourR = 125;
unsigned int colourG = 125;
unsigned int colourB = 125;
int brightness = 15;
int OK_CHRIS_DAY = 23;
int OK_CHRIS_MONTH = 9;
int OK_CHRIS_YEAR = 2018;

#define up 1
#define select 2

#define char_height 7
#define char_width 5

#define eeprom_WLAN_SSID 0
#define eeprom_WLAN_PASS 1
#define eeprom_Brightness 2
#define eeprom_Color 3
#define eeprom_OK_CHRIS_DAY 4
#define eeprom_OK_CHRIS_MONTH 5
#define eeprom_OK_CHRIS_YEAR 6

// Display Kram
Ticker display_ticker;
#define P_LAT 16
#define P_A 5
#define P_B 4
#define P_C 15
#define P_D 12
#define P_E 0
#define P_OE 2
#define matrix_width 64
#define matrix_height 32
uint8_t display_draw_time = 60;             //30-70 is usually fine
PxMATRIX display(64, 32, P_LAT, P_OE, P_A, P_B, P_C, P_D);

// Some standard colors
uint16_t colour00 = display.color565(255,255,255);
uint16_t colour01 = display.color565(255,255,125);
uint16_t colour02 = display.color565(255,255,0);
uint16_t colour03 = display.color565(255,125,0);
uint16_t colour04 = display.color565(255,0,0);
uint16_t colour05 = display.color565(255,0,125);
uint16_t colour06 = display.color565(255,125,125);
uint16_t colour07 = display.color565(255,125,255);
uint16_t colour08 = display.color565(125,125,255);
uint16_t colour09 = display.color565(125,125,125);
uint16_t colour10 = display.color565(125,125,0);
uint16_t colour11 = display.color565(0,125,0);
uint16_t colour12 = display.color565(0,125,125);
uint16_t colour13 = display.color565(0,125,255);
uint16_t colour14 = display.color565(0,0,255);
uint16_t colour15 = display.color565(0,0,125);
uint16_t colour16 = display.color565(125,0,125);
uint16_t colour17 = display.color565(125,0,0);
uint16_t colour18 = display.color565(255,0,0);
uint16_t colour19 = display.color565(255,0,255);
uint16_t colour20 = display.color565(125,0,255);

uint16_t newCOLORS[21] = {colour00, colour01, colour02, colour03, colour04, colour05, colour06, colour07, colour08, colour09, colour10, colour11, colour12, colour13, colour14, colour15, colour16, colour17, colour18, colour19, colour20};
int choosen_colour = 0;

//UDP
WiFiUDP Udp;                                // UDP Variable
unsigned int localPort = 123;               // Lokaler Port

//NTP Server
char ntpServerName1[] = "ntp1.t-online.de";
char ntpServerName2[] = "time.nist.gov";

TimeChangeRule CEST = { "CEST", Last, Sun, Mar, 2, 120 };     //Central European Summer Time
TimeChangeRule CET = { "CET ", Last, Sun, Oct, 3, 60 };       //Central European Standard Time
Timezone CE(CEST, CET);
TimeChangeRule *tcr;                                          //pointer to the time change rule, use to get the TZ abbrev
time_t utc, local, OK_CHRIS_TIME, diff;
const int NTP_PACKET_SIZE = 48;                               // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE];                           //buffer to hold incoming & outgoing packets
String tmp_time = "";
String tmp_date = "";

struct MyEEPROMStruct {
  char wifiSSID[100];
  char wifiPASS[100];
  int brightness;
  int colour;
  int dateDay;
  int dateMonth;
  int dateYear;
}eepEntry, eepTmp;
//  -------------------------------------------
//  -------------------------------------------
//  -------------------------------------------



//  -------------------------------------------
//  Hilfsfunktionen
//  -------------------------------------------



//  -------------------------------------------
// ISR for display refresh
//  -------------------------------------------
void display_updater(){
  display.display(display_draw_time);
}
//  -------------------------------------------



//  -------------------------------------------
// Update Funktion für... keine Ahnung?
//  -------------------------------------------
void display_update_enable(bool is_enable){
  if (is_enable)
    display_ticker.attach(0.004, display_updater);
  else
    display_ticker.detach();
}
//  -------------------------------------------



//  -------------------------------------------
//  Eingabe über serielle Schnittstelle holen
//  -------------------------------------------
String GetInput(String message){
  String tmp = "";
  String input = "";
  Serial.println(message);
  do {
    input = Serial.readString();
  } while (input == "");
  input.trim();
  tmp = "Input: " + input;
  Serial.println(tmp);
  return input;
}
//  -------------------------------------------


//  -------------------------------------------
//  Hole NTP Zeit
//  -------------------------------------------
bool getNtpTime(char* ntpServerName){
  //Serial.print(F("NTP request..."));
  if (timeStatus() == timeSet) {
    //Serial.println(F("not necessary"));
    return true;
  }

  IPAddress ntpServerIP; // NTP server's ip address

  while (Udp.parsePacket() > 0); // discard any previously received packets
  Serial.println(F("Transmit NTP Request"));
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println(F("Receive NTP Response"));
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 = (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      setTime(secsSince1900 - 2208988800UL);
      //setTime(23, 55, 0, 30, 3, 2016); //simulate time for test
      return true;
    }
  }
  Serial.println(F("FATAL ERROR : No NTP Response."));
  return false; // return 0 if unable to get the time
}
//  -------------------------------------------



//  -------------------------------------------
//  send an NTP request to the time server at the given address
//  -------------------------------------------
void sendNTPpacket(IPAddress &address){
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}
//  -------------------------------------------



//  -------------------------------------------
//  Function to return the compile date and time as a time_t value
//  -------------------------------------------
time_t compileTime(void){
#define FUDGE 25        //fudge factor to allow for compile time (seconds, YMMV)
  char *compDate = __DATE__, *compTime = __TIME__, *months = "JanFebMarAprMayJunJulAugSepOctNovDec";
  char chMon[3], *m;
  int d, y;
  tmElements_t tm;
  time_t t;
  strncpy(chMon, compDate, 2);
  chMon[3] = '\0';
  m = strstr(months, chMon);
  tm.Month = ((m - months) / 3 + 1);
  tm.Day = atoi(compDate + 4);
  tm.Year = atoi(compDate + 7) - 1970;
  tm.Hour = atoi(compTime);
  tm.Minute = atoi(compTime + 3);
  tm.Second = atoi(compTime + 6);
  t = makeTime(tm);
  return t + FUDGE;        //add fudge factor to allow for compile time
}
//  -------------------------------------------



//  -------------------------------------------
// Erstelle Zeitdifferenz
//  -------------------------------------------
int dateDiff(int year1, int mon1, int day1, int year2, int mon2, int day2)
{
    int ref,dd1,dd2,i;
    ref = year1;
    if(year2<year1)
    ref = year2;
    dd1=0;
    dd1=dater(mon1);
    for(i=ref;i<year1;i++)
    {
        if(i%4==0)
        dd1+=1;
    }
    dd1=dd1+day1+(year1-ref)*365;
    dd2=0;
    for(i=ref;i<year2;i++)
    {
        if(i%4==0)
        dd2+=1;
    }
    dd2=dater(mon2)+dd2+day2+((year2-ref)*365);
    return dd2-dd1;
}

int dater(int x)
{ const int dr[]= { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
  return dr[x-1];
}

String getDiff(int year1, int mon1, int day1, int year2, int mon2, int day2){
  int diff = dateDiff(year1,mon1, day1, year2, mon2, day2);
  int tmp_year, tmp_month, tmp_day;

  tmp_year = diff / 356;
  tmp_month = (diff % 356) / 31;
  tmp_day = (diff - tmp_year * 356 - tmp_month * 31);

  char tmp_buff[9];
  sprintf(tmp_buff, "%02d.%02d.%04d", tmp_day, tmp_month, tmp_year);
  return tmp_buff;
}
//  -------------------------------------------



//  -------------------------------------------
//  Formatiere einzelne Nummern in anzeigbaren String
//  -------------------------------------------
String stringDigits(int val, bool collon = true){
  String tmp = "";
  
  if(collon) tmp = tmp + ':';  
  if (val < 10)tmp = tmp + '0';
  
  tmp = tmp + String(val);
  return tmp;
}
//  -------------------------------------------



//  -------------------------------------------
//  Formatiere Zeit in anzeigbaren String
//  -------------------------------------------
String stringTime(time_t t){
  return stringDigits(hour(t), false) + stringDigits(minute(t)); //+ stringDigits(second(t));
}
//  -------------------------------------------



//  -------------------------------------------
//  Formatiere Datum in anzeigbaren String
//  -------------------------------------------
String stringDate(time_t t){
  return stringDigits(day(t), false) + '.' + stringDigits(month(t), false) + '.' + year(t);
}
//  -------------------------------------------



//  -------------------------------------------
//  Schauen ob WLAN verbunden ist
//  -------------------------------------------
bool isConnected(long timeOutSec) {
  timeOutSec = timeOutSec * 1000;
  int z = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print(".");
    if (z == timeOutSec / 200) {
      return false;
    }
    z++;
  }
  return true;
}
//  -------------------------------------------



//  -------------------------------------------
//  Erstelle Zufallsfarbe
//  -------------------------------------------
unsigned int randomColour(unsigned int colour){
  unsigned int tmp;

  tmp = rand()%(25-(-25) + 1) + (-25);
  
  if(tmp + colour  < 0 || tmp + colour > 255){
    tmp = 0;
  }
  return tmp + colour;
}
//  -------------------------------------------



//  -------------------------------------------
//  Setup
//  -------------------------------------------
void setup() {

  // Variablen
  bool bootmenu = false;
  unsigned long t1, t2;
  unsigned int boot_time_tmp = 0;

  // Starte serielle Schnittstelle und setze Begrüßung
  Serial.begin(9600);
  Serial.println("GREETINGS PROFESSOR FALKEN");
  
  // Aktiviere EEPROM
  //EEPROM.begin(256);
  EEPROM.begin(sizeof(eepEntry));

  if(EEPROM.percentUsed() >= 0){
    EEPROM.get(0, eepEntry);
    Serial.println("EEPROM has data from a previous run.");
    Serial.print(EEPROM.percentUsed());
    Serial.println("% of ESP flash space currently used");
  } else {
    Serial.println("EEPROM size changed - EEPROM data zeroed - commit() to make permanent");    
  }

  // Display
  display.begin(16);
  display.clearDisplay();
  display.setFastUpdate(true);
  display_update_enable(true);

  // Abfrage ob Einstellungen gemacht werden sollen
  Serial.println("Please send a ""y"" to change settings");

  // Setze Zeit
  t1 = millis();
  
  do {
    // Lese serielle Schnittstelle
    buff = Serial.readString();

    // Lösche alle unnötigen Leerzeichen
    buff.trim();

    // Schaue ob Eingabe korrekt ist
    if (buff.startsWith("y")) {
      bootmenu = true;
    }

    // Setze Zeit
    t2 = millis();
    
    // Fange verbleibende Zeit ab und Schreibe einen gescheiten Wert
    if ((boot_time - t2 - t1) / 1000 > boot_time / 1000) {
      boot_time_tmp = 0;
    } else {
      boot_time_tmp = (boot_time - t2 - t1) / 1000;
    }

    // Ausgabe von Restzeit
    buff = "Remaining Time: " + String(boot_time_tmp);
    Serial.println(buff);
  
  } while (t2 - t1 < boot_time and bootmenu == false);

  // Schaue ob Bootvorgang unterbrochen und Eingaben stattfinden sollen
  if (bootmenu) {
    // WLAN Settings Abfragen
    WLAN_SSID = GetInput("Please enter WLAN SSID");
    WLAN_PASSWORD = GetInput("Please enter WLAN Password");
    buff = GetInput("Please Enter Day where you have met");
    eepEntry.dateDay = buff.toInt();
    buff = GetInput("Please enter Month where you have met");
    eepEntry.dateMonth = buff.toInt();
    buff = GetInput("Please enter Year where you have met");
    eepEntry.dateYear = buff.toInt();

    // Ausgabe der eingegebenen Daten
    Serial.println("Following data have been entered");
    buff = "SSID: " + WLAN_SSID;
    Serial.println(buff);
    buff = "PASS: " + WLAN_PASSWORD;
    Serial.println(buff);
    
    // Schreiben von Daten
    WLAN_SSID.toCharArray(eepEntry.wifiSSID, WLAN_SSID.length() + 1);
    WLAN_PASSWORD.toCharArray(eepEntry.wifiPASS, WLAN_PASSWORD.length() + 1);
    EEPROM.put(0, eepEntry);
    EEPROM.commit();
  }

  // Hole Daten aus EEPROM
  WLAN_SSID = eepEntry.wifiSSID;
  WLAN_PASSWORD = eepEntry.wifiPASS;

  // Anzeige mit welchem Netz verbunden wird
  buff = "Connecting to [" + WLAN_SSID + "]/[" + WLAN_PASSWORD + "]";
  Serial.println(buff);

  // Setup für WLAN
  WiFi.mode(WIFI_STA);
  WiFi.begin(WLAN_SSID, WLAN_PASSWORD);
  buff = "WLAN_SSID: " + String(eepEntry.wifiSSID);
  Serial.println(buff);
  buff = "WLAN_PASS: " + String(eepEntry.wifiPASS);
  Serial.println(buff);

  // Ausgabe von MAC addresse
  buff = "MAC Adress :" + WiFi.macAddress();
  Serial.println(buff);

  // Statusanzeige für WLAN Verbindung
  if (isConnected(30)) {
    wasConnected = true;
    Serial.println(F("Starting UDP"));
    Udp.begin(localPort);
    Serial.print(F("Local port: "));
    Serial.println(Udp.localPort());
    Serial.println(F("waiting for sync"));
  }

  // Ausgabe bei Verbindung
  Serial.println("WiFi connected");
  buff = "IP Adress: " + WiFi.localIP().toString();
  Serial.println(buff);

  // Lese Werte
  brightness = eepEntry.brightness;
  buff = "Read brightness of: " + String(brightness);
  Serial.println(buff);
  choosen_colour = eepEntry.colour;
  buff = "Read Colourscheme of: " + String(choosen_colour);
  Serial.println(buff);
  OK_CHRIS_DAY = eepEntry.dateDay;
  OK_CHRIS_MONTH = eepEntry.dateMonth;
  OK_CHRIS_YEAR = eepEntry.dateYear;
  buff = "DateDate: "+ String(OK_CHRIS_DAY) + '.' + String(OK_CHRIS_MONTH) + '.' + String(OK_CHRIS_YEAR);
  Serial.println(buff);
}
//  -------------------------------------------


//  -------------------------------------------
//  Main Loop
//  -------------------------------------------
void loop() {
  // --------------------------------------------------------
  // Variablen
  // --------------------------------------------------------
  unsigned long currentMillis = millis();
  bool update_menu = false;
  // --------------------------------------------------------


  // --------------------------------------------------------
  // NTP Kram
  // --------------------------------------------------------
  if (currentMillis - NTP_Millis >= 5000) {
    
    NTP_Millis = currentMillis;

    // Schauen ob Verbunden ist
    if (!isConnected(10) && wasConnected) {
      delay(200);
      ESP.restart();
    }

    // Wechsel den Server wenn einer nicht verfügbar ist
    if (!getNtpTime(ntpServerName1)) {
      getNtpTime(ntpServerName2);
    }

    // Hole mir die aktuelle Zeit
    local = CE.toLocal(now(), &tcr);

    // Schaue ob geupdatet werden muss
    if(tmp_time != stringTime(local) ||tmp_date != stringDate(local)){
      update_menu = true;
      if(eepEntry.brightness != brightness){ //|| eepEntry.colour != choosen_colour){
        Serial.println("Update Struct, because of change of colour or brightness");
        eepEntry.brightness = brightness;
        eepEntry.colour = choosen_colour;
        EEPROM.put(0, eepEntry);
        EEPROM.commit();
      }
    }
  }
  // --------------------------------------------------------



  // --------------------------------------------------------
  // Hole mir aktuellen Schalterzustand
  // --------------------------------------------------------
  sensorValue = analogRead(analogInPin);
  outputValue = map(sensorValue, 0, 1023, 0, 255);

  if (outputValue >= 36 && outputValue <= 46) {
    switch_tmp = up;
  }

  if (outputValue >= 50 && outputValue <= 60) {
    switch_tmp = select;
  }

  if (outputValue < 5 && switch_tmp > 0) {
    switch_val = switch_tmp;
    switch_tmp = 0;
  }
  // --------------------------------------------------------



  // --------------------------------------------------------
  // Setze Helligkeit
  // --------------------------------------------------------
  if(switch_val == up){
    if(brightness+25 > 255){
      brightness = 15;
    }else{
      brightness += 25;
    }
  }

  display.setBrightness(brightness);
  // --------------------------------------------------------



  // --------------------------------------------------------
  // Setze Zufallsfarbe
  // --------------------------------------------------------
  if(switch_val == select){
    if(choosen_colour+1 > sizeof(newCOLORS)){
      choosen_colour = 0;
    }else{
      choosen_colour++;
    }
    update_menu = true;
  }
  
  display.setTextColor(newCOLORS[choosen_colour]);
  // --------------------------------------------------------



  // --------------------------------------------------------
  // Display
  // --------------------------------------------------------
  if(update_menu){
    display.clearDisplay();  
    display.setCursor(5,6);
    display.setFont(&FreeMonoBold9pt7b);
    display.println(stringTime(local));
    display.setFont();
    display.setCursor(3,15);
    display.println(stringDate(local));
    display.setFont();
    display.setCursor(3,24);
    display.println(getDiff(OK_CHRIS_YEAR, OK_CHRIS_MONTH, OK_CHRIS_DAY, year(local), month(local), day(local)));
  }
  // --------------------------------------------------------



  // --------------------------------------------------------
  // Setze Schalter zurück
  // --------------------------------------------------------
  switch_val = 0;
  // --------------------------------------------------------


 
  // --------------------------------------------------------
  // Schreibe Zeiten
  // --------------------------------------------------------
  tmp_time = stringTime(local);
  tmp_date = stringDate(local);
  // --------------------------------------------------------
  
  update_menu = false;
}
